//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TP2_BD2_H2019
{
    using System;
    using System.Collections.Generic;
    
    public partial class Superviseur
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Superviseur()
        {
            this.Notes = new HashSet<Note>();
            this.Supervisions = new HashSet<Supervision>();
        }
    
        public int SuperviseurID { get; set; }
        public Nullable<int> PersonneID { get; set; }
        public Nullable<int> ProgrammeID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Note> Notes { get; set; }
        public virtual Personne Personne { get; set; }
        public virtual Programme Programme { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Supervision> Supervisions { get; set; }
    }
}

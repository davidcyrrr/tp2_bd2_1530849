﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * titre           :SuperviseurCtrl.cs
 * description     :Gestion de la classe Superviseur
 *                  Permet de faire le CRUD d'un Superviseur
 * author          :David Cyr
 * date de création:20190409
 * usage           :
 * notes           :
 */

namespace TP2_BD2_H2019
{
    class SuperviseurCtrl:UtilisateursCtrl
    {
        /// <summary>
        /// Le menu principal pour la gestion des superviseurs
        /// </summary>
        /// <param name="context"></param>
        public void MenuPrincipal(TP2_BD2_1530849Entities1 context)
        {
            List<String> optionsMenu = new List<String>();
            optionsMenu.Add("1) Lister tous les superviseur");
            optionsMenu.Add("2) Ajouter un superviseur");
            optionsMenu.Add("3) Effacer un superviseur");
            optionsMenu.Add("4) modifier un superviseur");
           

            optionsMenu.Add("0) sortir");

            View menu = new View();
            int choix;
            do
            {
                menu.AfficheMenu(optionsMenu);
                choix = menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4, 5 });

                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            this.Lister(context);
                            break;
                        case 2:
                            this.Ajouter(context);
                            break;
                        case 3:
                            this.Effacer(context);
                            break;
                        case 4:
                            this.Modifier(context);
                            break;
                    }
                    Console.WriteLine("");
                }
            } while (choix != 0);
        }





        /// <summary>
        /// Affiche les infos des superviseurs
        /// </summary>
        /// <param name="unContact">le contact à afficher</param>
        private void Lister(TP2_BD2_1530849Entities1 context)
        {
            foreach (Superviseur Superviseur in context.Superviseurs)
            {
                Console.WriteLine("ID du superviseur: {0}  ID de l'utilisateur: {1} ID du programme {2}", Superviseur.SuperviseurID, Superviseur.PersonneID, Superviseur.ProgrammeID);
            }

        }

        /// <summary>
        /// Ajout d'un nouveau superviseur
        /// </summary>
        /// <param name="context">la connection à la bd</param>
        private void Ajouter(TP2_BD2_1530849Entities1 context)
        {
            View view = new View();
            
            

            Personne nouvellePersonne = new Personne();

            nouvellePersonne.PersonneID = view.InputInt("ID Personne (utilisateur) du superviseur : ");
            nouvellePersonne.Nom = view.InputString("Nom du superviseur : ");
            nouvellePersonne.Prenom = view.InputString("Prenom du superviseur : ");
            nouvellePersonne.Courriel = view.InputString("Courriel du nouveau superviseur : ");
            nouvellePersonne.Telephone = view.InputString("Telephone du nouveau superviseur : ");
            nouvellePersonne.Password = view.InputString("Password du nouveau superviseur : ");

            Superviseur nouveauSuperviseur = new Superviseur();

            nouveauSuperviseur.SuperviseurID = view.InputInt("ID du nouveau superviseur : ");            

            int choix = view.InputInt("Id du programme à associer : ");

            Personne personneAssocie = context.Personnes.Find(nouvellePersonne.PersonneID);
            Programme programmeAssocie = context.Programmes.Find(choix);
            if (programmeAssocie == null)
            {
                Console.WriteLine("mauvais id");
            }
            else
            {
                nouveauSuperviseur.ProgrammeID = choix;
                nouveauSuperviseur.PersonneID = nouvellePersonne.PersonneID;
                // confirmation et ajout dans la BD
                Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");
                
                char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                if (sauvegarder == 'O')
                {
                    context.Superviseurs.Add(nouveauSuperviseur);
                    context.Personnes.Add(nouvellePersonne);
                    context.SaveChanges();
                }
            }

        }
        /// <summary>
        /// Efface un superviseur
        /// </summary>
        /// <param name="unContact">le contact à afficher</param>
        private void Effacer(TP2_BD2_1530849Entities1 context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id du superviseur à effacer (0 pour annuler) :");
            if (choix != 0)
            {
                Superviseur superviseurAEffacer = context.Superviseurs.Find(choix);
                
                if (superviseurAEffacer != null)
                {
                    
                    Char effacer = view.InputChar("Effacer ce département O/N ", new List<char> { 'O', 'N' }, true);
                    if (effacer == 'O')
                    {
                        context.Personnes.Remove(superviseurAEffacer.Personne);
                        context.Superviseurs.Remove(superviseurAEffacer);                        
                        context.SaveChanges();
                    }

                }
                else
                {
                    Console.WriteLine("Mauvais id");
                }
            }

        }
        /// <summary>
        /// Modifier un superviseur existant
        /// </summary>
        /// <param name="context">la connection à la bd</param>
        private void Modifier(TP2_BD2_1530849Entities1 context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id du superviseur à modifier (0 pour annuler) : ");
            if (choix != 0)
            {
                Superviseur Superviseur = context.Superviseurs.Find(choix);
                if (Superviseur != null)
                {

                    
                    Superviseur.PersonneID = view.InputInt("Id de la personne: ", Superviseur.PersonneID);
                    Superviseur.ProgrammeID = view.InputInt("Id du programme: ", Superviseur.ProgrammeID);

                    // confirmation et ajout dans la BD
                    Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");
                    Console.WriteLine("{0} {1}",
                        Superviseur.PersonneID,
                        Superviseur.ProgrammeID
                        );
                    char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                    if (sauvegarder == 'O')
                    {
                        context.SaveChanges();
                    }
                }
                else
                {
                    Console.WriteLine("Mauvais id");
                }
            }

        }

    }

   


}

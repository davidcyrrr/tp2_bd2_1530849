﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * titre           :EtudiantCtrl.cs
 * description     :Gestion de la classe Etudiant
 *                  Permet de faire le CRUD d'un etudiant
 * author          :david cyr
 * date de création:20190409
 * usage           :
 * notes           :
 */

namespace TP2_BD2_H2019
{
    class EtudiantCtrl : UtilisateursCtrl
    {
        /// <summary>
        /// Le menu principal pour la gestion des etudiants
        /// </summary>
        /// <param name="context"></param>
        public void MenuPrincipal(TP2_BD2_1530849Entities1 context)
        {
            List<String> optionsMenu = new List<String>();
            optionsMenu.Add("1) Lister tous les etudiants");
            optionsMenu.Add("2) Ajouter un etudiant");
            optionsMenu.Add("3) Effacer un etudiant");
            optionsMenu.Add("4) modifier un etudiant");


            optionsMenu.Add("0) sortir");

            View menu = new View();
            int choix;
            do
            {
                menu.AfficheMenu(optionsMenu);
                choix = menu.ChoisirOption(new List<int> { 0, 1, 2, 3, 4, 5 });

                if (choix != 0)
                {
                    switch (choix)
                    {
                        case 1:
                            this.Lister(context);
                            break;
                        case 2:
                            this.Ajouter(context);
                            break;
                        case 3:
                            this.Effacer(context);
                            break;
                        case 4:
                            this.Modifier(context);
                            break;
                    }
                    Console.WriteLine("");
                }
            } while (choix != 0);
        }





        /// <summary>
        /// Affiche les infos des etudiants
        /// </summary>
        /// <param name="unContact">l'etudiant à afficher</param>
        private void Lister(TP2_BD2_1530849Entities1 context)
        {
            foreach (Etudiant Etudiant in context.Etudiants)
            {
                Console.WriteLine("ID du Etudiant: {0}  ID de l'utilisateur: {1} Etat {2}", Etudiant.EtudiantID, Etudiant.PersonneID, Etudiant.Etat);
            }
        }

        /// <summary>
        /// Ajout d'un nouvel etudiant
        /// </summary>
        /// <param name="context">la connection à la bd</param>
        private void Ajouter(TP2_BD2_1530849Entities1 context)
        {
            View view = new View();



            Personne nouvellePersonne = new Personne();

            nouvellePersonne.PersonneID = view.InputInt("ID Personne (utilisateur) de l'etudiant : ");
            nouvellePersonne.Nom = view.InputString("Nom de l'etudiant : ");
            nouvellePersonne.Prenom = view.InputString("Prenom de l'etudiant : ");
            nouvellePersonne.Courriel = view.InputString("Courriel de l'etudiant : ");
            nouvellePersonne.Telephone = view.InputString("Telephone de l'etudiant : ");
            nouvellePersonne.Password = view.InputString("Password de l'etudiant : ");

            Etudiant nouvelEtudiant = new Etudiant();

            nouvelEtudiant.EtudiantID = view.InputInt("ID du nouvel etudiant : ");
            nouvelEtudiant.Etat = view.InputString("Etat du nouvel etudiant : ");

            Personne personneAssocie = context.Personnes.Find(nouvellePersonne.PersonneID);



            nouvelEtudiant.PersonneID = nouvellePersonne.PersonneID;
            // confirmation et ajout dans la BD

            Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");

                char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                if (sauvegarder == 'O')
                {
                    context.Etudiants.Add(nouvelEtudiant);
                    context.Personnes.Add(nouvellePersonne);
                    context.SaveChanges();
                }
            

        }
        /// <summary>
        ///  Efface un etudiant
        /// </summary>
        /// <param name="unContact">le contact à afficher</param>
        private void Effacer(TP2_BD2_1530849Entities1 context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id de l'etudiant à effacer (0 pour annuler) :");
            if (choix != 0)
            {
                Etudiant etudiantAEffacer = context.Etudiants.Find(choix);
                
                if (etudiantAEffacer != null)
                {

                    Char effacer = view.InputChar("Effacer ce département O/N ", new List<char> { 'O', 'N' }, true);
                    if (effacer == 'O')
                    {
                        context.Personnes.Remove(etudiantAEffacer.Personne);
                        context.Etudiants.Remove(etudiantAEffacer);
                        context.SaveChanges();
                    }

                }
                else
                {
                    Console.WriteLine("Mauvais id");
                }
            }

        }
        /// <summary>
        /// Modifier un etudiant existant
        /// </summary>
        /// <param name="context">la connection à la bd</param>
        private void Modifier(TP2_BD2_1530849Entities1 context)
        {
            this.Lister(context);
            View view = new View();
            int choix = view.InputInt("Id du etudiant à modifier (0 pour annuler) : ");
            if (choix != 0)
            {
                Etudiant Etudiant = context.Etudiants.Find(choix);
                if (Etudiant != null)
                {


                    Etudiant.PersonneID = view.InputInt("Id de la personne: ", Etudiant.PersonneID);
                    Etudiant.Etat = view.InputString("Id du programme: ", Etudiant.Etat);

                    // confirmation et ajout dans la BD
                    Console.WriteLine("Voulez-vous vraiment sauvegarder ces valeurs :");
                    Console.WriteLine("{0} {1}",

                        Etudiant.PersonneID,
                        Etudiant.Etat
                        );
                    char sauvegarder = view.InputChar("O/N", new List<char> { 'O', 'N' }, true);
                    if (sauvegarder == 'O')
                    {
                        context.SaveChanges();
                    }
                }
                else
                {
                    Console.WriteLine("Mauvais id");
                }
            }

        }

    }




}

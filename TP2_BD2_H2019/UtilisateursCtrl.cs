﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security.Cryptography;

/*
 * titre           :UtilisateurCtrl.cs
 * description     :Gestion de la classe Utilisateur
 *                  Cette classe est une classe abstraite
 *                  Elle permet de gérer le mot de passe ainsi que
 *                  les champs propres à un utilisateur
 * author          :Benoit Desrosiers
 * date de création:20190314
 * usage           :
 * notes           :
 */
namespace TP2_BD2_H2019
{
    abstract class UtilisateursCtrl
    {

        /// <summary>
        /// Encrypte une chaine de caractère de la même façon que SqlServer le fait
        /// </summary>
        /// <param name="motAEncoder">La chaine de caractère à encrypter</param>
        /// <returns>motAEncoder encrypté</returns>
        /// 
        /// Cette fonction devrait probablement aller dans une librairie de helper
        /// Mais j'en ai pas pour l'instant. 
        /// 
        protected static string EncodeMotDePasse(string motAEncoder)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.Unicode.GetBytes(motAEncoder));
                return Encoding.Default.GetString(hash);
            }

        }

        /// <summary>
        /// Demande les infos d'un utilisateur
        /// </summary>
        /// <param name="view">La View à utiliser pour l'affichage</param>
        /// <param name="unUtilisateur">L'utilisateur à modifier. Est changé par la méthode </param>
        protected void InputInfoUtilisateur(View view, Utilisateur unUtilisateur)
        {
            unUtilisateur.prenom = view.InputString("Prenom :");
            unUtilisateur.nom = view.InputString("Nom :");
            unUtilisateur.login = view.InputString("Login :");
            unUtilisateur.motDePasse = EncodeMotDePasse(view.InputString("Mot de passe :"));
            unUtilisateur.telephone = view.InputString("Téléphone :");
            unUtilisateur.courriel = view.InputString("Courriel: ");
            ;
        }

    }
}

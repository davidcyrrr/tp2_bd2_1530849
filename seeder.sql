
DELETE FROM dbo.Note
GO

DELETE FROM dbo.Supervision
GO

DELETE FROM dbo.Programme_Etudiant
GO

DELETE FROM dbo.Offre
GO

DELETE FROM dbo.Session
GO

DELETE FROM dbo.ApplicationAuPoste
GO

DELETE FROM dbo.LettrePresentation
GO

DELETE FROM dbo.CV
GO

DELETE FROM dbo.Etudiant
GO

DELETE FROM dbo.Contacte
GO

DELETE FROM dbo.Departement
GO

DELETE FROM dbo.Entreprise
GO

DELETE FROM dbo.Superviseur
GO

DELETE FROM dbo.Programme
GO

DELETE  FROM dbo.Personne
GO
INSERT INTO dbo.Personne (PersonneID, Nom, Prenom, Courriel, Telephone, Password) VALUES
('1','Komisarec', 'Mike', 'MIKOhabs54@email.com','819-1234-432', 'BetterThanWeber1234'),
('2','Philips', 'Trevor','ILYW@email.com','1800-666-6969', 'Mama-Genesis666'),
('3','Hanks', 'Tom','SavingPrivateRyan@gmail.com','819-819-8198', 'Forest_Gump-wqihgd'),
('4','Stalin', 'Joseph','MotherRussia@email.com','019-430-2020', 'BTG1942J17'),
('5','Guarnere', 'Bill','Easydoesit@hotmail.com','019-440-6050', 'SOC1992'),
('6','Rocky', 'A$AP','PetitBurger@email.com','819-918-1111', 'GinNJuiceSD139284'),
('7','Tremblay', 'Jennifer','LALife@gmail.com','1800-777-7777', 'paodjs2553'),
('8','Joncas', 'Maelie','Whynotcoconut@email.com','20-100', 'euhh23232165'),
('9','Gagne', 'Jean-Micheal','Jemcee@email.com','111-111-1111', 'JNPDI1234554321')

INSERT INTO dbo.Programme(ProgrammeID, Nom, Sigle) VALUES
('1','Informatique','420-DM'),
('2','Bureautique','430-DE'),
('3','Evaluation_en_batiments','310_AC')

INSERT INTO dbo.Superviseur(SuperviseurID, [PersonneID], [ProgrammeID]) VALUES
('1', ( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'MIKOhabs54@email.com'), (SELECT ProgrammeID FROM dbo.Programme WHERE Sigle  = '420-DM')),
('2', ( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'ILYW@email.com'), (SELECT ProgrammeID FROM dbo.Programme WHERE Sigle  = '430-DE')),
('3',( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'SavingPrivateRyan@gmail.com'), (SELECT ProgrammeID FROM dbo.Programme WHERE Sigle  = '420-DM'))

INSERT INTO dbo.Entreprise(EntrepriseID,NomEntreprise, TelephoneEntreprise) VALUES
('1','Transport_Coutu_Inc','819-472-3245'),
('2','SNC_Lavalois','819-473-4356'),
('3','R_Media','819-474-1010')

INSERT INTO dbo.Departement(DepartementID, [EntrepriseID], NomDepartement, TelephoneDepartement) VALUES
('1', ( SELECT EntrepriseID FROM dbo.Entreprise WHERE NomEntreprise = 'Transport_Coutu_Inc'),'Vente','819-472-3246'),
('2', ( SELECT EntrepriseID FROM dbo.Entreprise WHERE NomEntreprise = 'Transport_Coutu_Inc'),'Transport','819-472-3247'),
('3', ( SELECT EntrepriseID FROM dbo.Entreprise WHERE NomEntreprise = 'SNC_Lavalois'),'Plainte','819-473-4357')

INSERT INTO dbo.Contacte(ContacteID, [PersonneID], [DepartementID]) VALUES
('1', ( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'MotherRussia@email.com'), (SELECT DepartementID FROM dbo.Departement WHERE TelephoneDepartement = '819-472-3246')),
('2', ( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'Easydoesit@hotmail.com'), (SELECT DepartementID FROM dbo.Departement WHERE TelephoneDepartement = '819-472-3247')),
('3', ( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'PetitBurger@email.com'), (SELECT DepartementID FROM dbo.Departement WHERE TelephoneDepartement = '819-473-4357'))

INSERT INTO dbo.Etudiant(EtudiantID, [PersonneID], Etat) VALUES
('1', ( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'LALife@gmail.com'),'Admissible'),
('2', ( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'Whynotcoconut@email.com'),'Non_admissible'),
('3',( SELECT PersonneID FROM dbo.Personne WHERE Courriel = 'Jemcee@email.com'),'Demande_en_cours')

INSERT INTO dbo.CV(CVID, [EtudiantID], DateCV, EtatCV, PosteRechercher) VALUES
('1', ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '1'),'2-2-2012','Accepte','programmeur'),
('2', ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '2'),'3-12-2032','Non_accepte','secretaire'),
('3', ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '3'),'12-4-1634','En_cours','evaluateur_de_batiment')

INSERT INTO dbo.LettrePresentation(LettreID, [CVID], DateLettre, EtatLettre) VALUES
('1', ( SELECT CVID FROM dbo.CV WHERE CVID  = '1'),'2-3-2012','Accepte'),
('2', ( SELECT CVID FROM dbo.CV WHERE CVID  = '2'),'3-13-2032','Non_accepte'),
('3', ( SELECT CVID FROM dbo.CV WHERE CVID  = '3'),'12-5-1634','En_cours')

INSERT INTO dbo.ApplicationAuPoste (ApplicationID, [LettreID], DateAppli, EtatAppli) VALUES
('1', ( SELECT LettreID FROM dbo.LettrePresentation WHERE LettreID = '1'),'2-4-2012','Accepte'),
('2', ( SELECT LettreID FROM dbo.LettrePresentation WHERE LettreID = '2'),'3-14-2032','Non_accepte'),
('3', ( SELECT LettreID FROM dbo.LettrePresentation WHERE LettreID = '3'),'12-6-1634','En_cours')

INSERT INTO dbo.Session(SessionID, Session) VALUES
('1','Hiver_2012'),
('2','Automne_2032'),
('3','Hiver_1634')

INSERT INTO dbo.Offre (OffreID, [ApplicationID], [DepartementID], [SessionID]) VALUES
('1', ( SELECT ApplicationID FROM dbo.ApplicationAuPoste WHERE ApplicationID = '1'),
  ( SELECT DepartementID FROM dbo.Departement WHERE DepartementID = '1'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '1')),
('2', ( SELECT ApplicationID FROM dbo.ApplicationAuPoste WHERE ApplicationID = '2'),
  ( SELECT DepartementID FROM dbo.Departement WHERE DepartementID = '2'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '2')),
('3', ( SELECT ApplicationID FROM dbo.ApplicationAuPoste WHERE ApplicationID = '3'),
  ( SELECT DepartementID FROM dbo.Departement WHERE DepartementID = '3'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '3'))

INSERT INTO dbo.Programme_Etudiant ( [ProgrammeID], [EtudiantID], [SessionID]) VALUES
(( SELECT ProgrammeID FROM dbo.Programme WHERE ProgrammeID = '1'),
  ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '1'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '1')),
(( SELECT ProgrammeID FROM dbo.Programme WHERE ProgrammeID = '2'),
  ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '2'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '2')),
(( SELECT ProgrammeID FROM dbo.Programme WHERE ProgrammeID = '3'),
  ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '3'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '3'))

 INSERT INTO dbo.Supervision( [SuperviseurID], [EtudiantID], [SessionID]) VALUES
(( SELECT SuperviseurID FROM dbo.Superviseur WHERE SuperviseurID = '1'),
  ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '1'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '1')),
(( SELECT SuperviseurID FROM dbo.Superviseur WHERE SuperviseurID = '2'),
  ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '2'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '2')),
(( SELECT SuperviseurID FROM dbo.Superviseur WHERE SuperviseurID = '3'),
  ( SELECT EtudiantID FROM dbo.Etudiant WHERE EtudiantID = '3'),
  ( SELECT SessionID FROM dbo.Session WHERE SessionID = '3'))

  INSERT INTO dbo.Note(NoteID, [SuperviseurID], Note, [NoteApplicationID]) VALUES
('1', ( SELECT SuperviseurID FROM dbo.Superviseur WHERE SuperviseurID = '1'),
  'Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla',
  ( SELECT NoteID FROM dbo.Note WHERE NoteID = '')),
  ('2', ( SELECT SuperviseurID FROM dbo.Superviseur WHERE SuperviseurID = '1'),
  'Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla',
  ( SELECT NoteID FROM dbo.Note WHERE Note = 'Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla')),
  ('3', ( SELECT SuperviseurID FROM dbo.Superviseur WHERE SuperviseurID = '1'),
  'Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla',
  ( SELECT NoteID FROM dbo.Note WHERE NoteID = '1'))

  



 

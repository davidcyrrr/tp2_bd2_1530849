IF OBJECT_ID('Vue_PERSONNE_ETUDIANT') IS NOT NULL DROP VIEW Vue_PERSONNE_ETUDIANT;
GO
 
IF OBJECT_ID('dbo.Trigger_Note') IS NOT NULL DROP TRIGGER dbo.Trigger_Note;
GO

IF OBJECT_ID('dbo.Trigger_Log') IS NOT NULL DROP TABLE dbo.Trigger_Log;
GO

IF OBJECT_ID('dbo.Personne_Etudiant') IS NOT NULL DROP PROCEDURE dbo.Personne_Etudiant;
GO

IF OBJECT_ID('dbo.Note') IS NOT NULL DROP TABLE dbo.Note;
GO

IF OBJECT_ID('dbo.Supervision') IS NOT NULL DROP TABLE dbo.Supervision;
GO

IF OBJECT_ID('dbo.Programme_Etudiant') IS NOT NULL DROP TABLE dbo.Programme_Etudiant;
GO

IF OBJECT_ID('dbo.Offre') IS NOT NULL DROP TABLE dbo.Offre;
GO

IF OBJECT_ID('dbo.Session') IS NOT NULL DROP TABLE dbo.Session;
GO


IF OBJECT_ID('dbo.ApplicationAuPoste') IS NOT NULL DROP TABLE dbo.ApplicationAuPoste;
GO

IF OBJECT_ID('dbo.LettrePresentation') IS NOT NULL DROP TABLE dbo.LettrePresentation;
GO

IF OBJECT_ID('dbo.CV') IS NOT NULL DROP TABLE dbo.CV;
GO

IF OBJECT_ID('dbo.Etudiant') IS NOT NULL DROP TABLE dbo.Etudiant;
GO

IF OBJECT_ID('dbo.Contacte') IS NOT NULL DROP TABLE dbo.Contacte;
GO

IF OBJECT_ID('dbo.Departement') IS NOT NULL DROP TABLE dbo.Departement;
GO

IF OBJECT_ID('dbo.Entreprise') IS NOT NULL DROP TABLE dbo.Entreprise;
GO

IF OBJECT_ID('dbo.Superviseur') IS NOT NULL DROP TABLE dbo.Superviseur;
GO

IF OBJECT_ID('dbo.Programme') IS NOT NULL DROP TABLE dbo.Programme;
GO

IF OBJECT_ID('dbo.Personne') IS NOT NULL DROP TABLE dbo.Personne;
GO

CREATE TABLE dbo.Personne (
PersonneID			INT NOT NULL 
	CONSTRAINT PK_Personne_ID PRIMARY KEY,
Nom		nvarchar(60) NOT NULL,
Prenom		nvarchar(60) NOT NULL,
Courriel    nvarchar(60) NOT NULL UNIQUE,
Telephone   nvarchar(20) NOT NULL,
Password	nvarchar(60) NOT NULL,	
)
GO

CREATE TABLE dbo.Programme(
ProgrammeID	INT NOT NULL 
	CONSTRAINT PK_Programme_ID PRIMARY KEY,
Nom		    nvarchar(50) NOT NULL,
Sigle		    nvarchar(10) NOT NULL,


)
GO

CREATE TABLE dbo.Superviseur (
SuperviseurID			INT NOT NULL 
	CONSTRAINT PK_Superviseur_ID PRIMARY KEY,
PersonneID	int 
		CONSTRAINT FK_Superviseur_Personne_ID FOREIGN KEY (PersonneID)	
		REFERENCES dbo.Personne (PersonneID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
ProgrammeID	int 
		CONSTRAINT FK_Superviseur_Programme_ID FOREIGN KEY (ProgrammeID)	
		REFERENCES dbo.Programme (ProgrammeID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
)
GO

CREATE TABLE dbo.Entreprise  (
EntrepriseID		INT NOT NULL 
	CONSTRAINT PK_Entreprise_ID PRIMARY KEY,
NomEntreprise		nvarchar(255) NOT NULL UNIQUE,
TelephoneEntreprise nvarchar(30) NOT NULL UNIQUE,

)
GO

CREATE TABLE dbo.Departement  (
DepartementID		INT NOT NULL 
	CONSTRAINT PK_Departement_ID PRIMARY KEY,
EntrepriseID	int 
		CONSTRAINT FK_Departement_Entreprise_ID FOREIGN KEY (EntrepriseID)	
		REFERENCES dbo.Entreprise (EntrepriseID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
NomDepartement		nvarchar(255) NOT NULL,
TelephoneDepartement nvarchar(50) NOT NULL UNIQUE,

)
GO


CREATE TABLE dbo.Contacte  (
ContacteID		INT NOT NULL 
	CONSTRAINT PK_Contacte_ID PRIMARY KEY,
DepartementID	int 
		CONSTRAINT FK_Contacte_Departement_ID FOREIGN KEY (DepartementID)	
		REFERENCES dbo.Departement (DepartementID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
PersonneID	int 
		CONSTRAINT FK_Contacte_Personne_ID FOREIGN KEY (PersonneID)	
		REFERENCES dbo.Personne (PersonneID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,

)
GO

CREATE TABLE dbo.Etudiant (
EtudiantID			INT NOT NULL 
	CONSTRAINT PK_Etudiant_ID PRIMARY KEY,
PersonneID	int 
		CONSTRAINT FK_Etudiant_Personne_ID FOREIGN KEY (PersonneID)	
		REFERENCES dbo.Personne (PersonneID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
Etat            nvarchar(255) NOT NULL,

)
GO

CREATE TABLE dbo.CV (
CVID			INT NOT NULL 
	CONSTRAINT PK_CV_ID PRIMARY KEY,
EtudiantID	int 
		CONSTRAINT FK_CV_Etudiant_ID FOREIGN KEY (EtudiantID)	
		REFERENCES dbo.Etudiant (EtudiantID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
DateCV		     date NOT NULL,
EtatCV           nvarchar(50) NOT NULL,
PosteRechercher  nvarchar(255) NOT NULL,

)
GO

CREATE TABLE dbo.LettrePresentation (
LettreID	INT NOT NULL 
	CONSTRAINT PK_Lettre_ID PRIMARY KEY,
CVID	int 
		CONSTRAINT FK_Lettre_CV_ID FOREIGN KEY (CVID)	
		REFERENCES dbo.CV (CVID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
DateLettre		    date NOT NULL,
EtatLettre          nvarchar(50) NOT NULL,

)
GO

CREATE TABLE dbo.ApplicationAuPoste (
ApplicationID	INT NOT NULL 
	CONSTRAINT PK_Application_ID PRIMARY KEY,
LettreID	int 
		CONSTRAINT FK_Application_Lettre_ID FOREIGN KEY (LettreID)	
		REFERENCES dbo.LettrePresentation (LettreID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
DateAppli		    date NOT NULL,
EtatAppli         nvarchar(50) NOT NULL,

)
GO

CREATE TABLE dbo.Session(
SessionID	INT NOT NULL 
	CONSTRAINT PK_Session_ID PRIMARY KEY,
Session		    nvarchar(50) NOT NULL,



)
GO


CREATE TABLE dbo.Offre (
OffreID	INT NOT NULL 
	CONSTRAINT PK_Offre_ID PRIMARY KEY,
ApplicationID	int 
		CONSTRAINT FK_Offre_Application_ID FOREIGN KEY (ApplicationID)	
		REFERENCES dbo.ApplicationAuPoste (ApplicationID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
DepartementID	int 
		CONSTRAINT FK_Offre_Departement_ID FOREIGN KEY (DepartementID)	
		REFERENCES dbo.Departement (DepartementID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
SessionID	int 
		CONSTRAINT FK_Offre_Session_ID FOREIGN KEY (SessionID)	
		REFERENCES dbo.Session (SessionID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,


)
GO



CREATE TABLE dbo.Programme_Etudiant (
ProgrammeID	INT 
		CONSTRAINT FK_Programme_Etudiant_ID FOREIGN KEY (ProgrammeID)
		REFERENCES dbo.Programme (ProgrammeID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
SessionID	INT 
		CONSTRAINT FK_Session_Etudiant_ID FOREIGN KEY (SessionID)
		REFERENCES dbo.Session (SessionID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
EtudiantID	INT 
		CONSTRAINT FK_Etudiant_Programme_ID FOREIGN KEY (EtudiantID)
		REFERENCES dbo.Etudiant (EtudiantID)
		ON DELETE CASCADE
		ON UPDATE CASCADE
		CONSTRAINT PK_Programme_Etudiant_Session_ID PRIMARY KEY( ProgrammeID, SessionID, EtudiantID),

)
GO

CREATE TABLE dbo.Supervision (
SuperviseurID	INT 
		CONSTRAINT FK_Superviseur_Supervision_ID FOREIGN KEY (SuperviseurID)
		REFERENCES dbo.Superviseur (SuperviseurID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
SessionID	INT 
		CONSTRAINT FK_Session_Supervision_ID FOREIGN KEY (SessionID)
		REFERENCES dbo.Session (SessionID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
EtudiantID	INT 
		CONSTRAINT FK_Etudiant_Supervision_ID FOREIGN KEY (EtudiantID)
		REFERENCES dbo.Etudiant (EtudiantID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
		CONSTRAINT PK_Supervision_Etudiant_Session_ID PRIMARY KEY( SuperviseurID, SessionID, EtudiantID),

)
GO

CREATE TABLE dbo.Note(
NoteID	INT NOT NULL 
	CONSTRAINT PK_Note_ID PRIMARY KEY,
SuperviseurID	int 
		CONSTRAINT PK_Note_Superviseur_ID FOREIGN KEY (SuperviseurID)	
		REFERENCES dbo.Superviseur (SuperviseurID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
Note	    nvarchar(255) NOT NULL,
NoteApplicationID integer references Note(NoteID) 


)
GO

 CREATE PROCEDURE dbo.Personne_Etudiant @Courriel nvarchar(100)
as  
select * 
from dbo.Etudiant
JOIN dbo.Personne ON dbo.Personne.PersonneID = dbo.Etudiant.PersonneID
WHERE Courriel = @Courriel
GO




CREATE TABLE dbo.Trigger_Log(

Info_Log	    nvarchar(50) NOT NULL,
)
GO



CREATE TRIGGER TriggerNote on dbo.Note
AFTER INSERT,  UPDATE, DELETE AS
BEGIN
	INSERT INTO dbo.Trigger_Log(Info_Log) VALUES
	('Une_Note_a_ete_detruite_ajoute_ou_modifie')
	
END
GO


CREATE VIEW Vue_PERSONNE_ETUDIANT AS
SELECT *
FROM  dbo.Supervision,dbo.Entreprise,
JOIN dbo.Personne ON dbo.Personne.PersonneID = dbo.Etudiant.PersonneID